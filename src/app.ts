import express from 'express';
import { createConnection } from 'typeorm';
import { Lookup } from './entity/Lookup';
import { importCsv } from './importCsv';
import typeormConfig from '../configLoader';
import { loadEnvVariables } from '../envLoader';

const env = loadEnvVariables();

const app = express();
const PORT = env.APP_PORT || 8080;

app.use("/public", express.static("public"));

createConnection(typeormConfig).then(async (connection) => {
  const welcome = `Server is running on http://localhost:${PORT}`;

  app.get('/list', async (req, res) => {
    try {
      const lookupRepository = connection.getRepository(Lookup);
      const lookups = await lookupRepository.find();
      const response = lookups.map(lookup => ({
        id: lookup.id,
        code: lookup.code,
        name: lookup.name,
        flag: lookup.flag,
        currency_symbol: lookup.currency_symbol
      }));

      res.json(response);

    } catch (error) {
      console.error('Error fetching data:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });

  app.get('/', async (req, res) => {
    res.json(welcome);
  });

  app.listen(PORT, () => {
    console.log(welcome);
    importCsv(connection);
  });
  
}).catch(error => {
  console.error('TypeORM connection error:', error);
});
