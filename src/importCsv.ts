//CSV dosyasını db'ye aktaran dosya
import 'reflect-metadata';
import { createConnection, Connection } from 'typeorm';
import { Lookup } from './entity/Lookup';
import csv from 'csv-parser';
import * as fs from 'fs';
import * as amqp from 'amqplib/callback_api';
import * as path from 'path'
import { loadEnvVariables } from '../envLoader';

const env = loadEnvVariables();

const connectWithRetry = async () => {
  let connection: Connection;
  try {
    connection = await createConnection();
    console.log('Database connected successfully');
    await importCsv(connection);
  } catch (err) {
    console.error('TypeORM connection error:', err);
    setTimeout(connectWithRetry, env.connectWithRetry_TimeOut);
  }
};

export const importCsv = async (connection: Connection) => {
  const lookupRepository = connection.getRepository(Lookup);

  const csvFilePath = path.join(__dirname, `../public/country.csv`)

  const dirExist = fs.existsSync(csvFilePath)
  
  if(dirExist){
    fs.createReadStream(csvFilePath)
    .pipe(csv())
    .on('data', async (row) => {
      try {
        const { id, code, name, flag, currency_symbol, ...extra } = row;
        const lookup = new Lookup();
        lookup.code = row.iso2;
        lookup.name = row.name;
        lookup.flag = row.emoji;
        lookup.currency_symbol = row.currency_symbol;
        lookup.order = parseInt(row.numeric_code);
        lookup.active = true;
        lookup.content = {
          iso3: row.iso3,
          phone_code: row.phone_code,
          capital: row.capital,
          currency: row.currency,
          currency_name: row.currency_name,
          tld: row.tld,
          native: row.native,
          region: row.region,
          region_id: row.region_id,
          subregion: row.subregion,
          subregion_id: row.subregion_id,
          nationality: row.nationality,
          timezones: row.timezones,
          latitude: row.latitude,
          longitude: row.longitude,
          emojiU: row.emojiU
        };
        lookup.version = parseInt(row.numeric_code);
        lookup.type = "country";

        await connection.transaction(async (transactionalEntityManager) => {
         const saver = await transactionalEntityManager.save(lookup);
         if(saver){
          console.log(`Successfully imported record with code ${lookup.name}`);
         } else {
          console.error('Error importing country.');
         }
        });

        amqp.connect('amqp://rabbitmq', (error0, connection) => {
          if (error0) {
            throw error0;
          }
          connection.createChannel((error1, channel) => {
            if (error1) {
              throw error1;
            }

            const exchange = 'log';
            const msg = JSON.stringify({
              event: 'CSV_IMPORT',
              data: lookup
            });

            channel.assertExchange(exchange, 'fanout', { durable: false });
            channel.publish(exchange, '', Buffer.from(msg));
          });

          setTimeout(() => {
            connection.close();
          }, env.connection_TimeOut);
        });
      } catch (error) {
        console.error('Error processing CSV row:', error);
      }
    })
    .on('end', () => {
      console.log('CSV file processing completed');
    })
    .on('error', (error) => {
      console.error('CSV processing error:', error);
    });
  } else {
    console.error('CSV file cannot be found');
  }  
};
