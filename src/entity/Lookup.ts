//Entity oluşturan dosya
import { Entity, PrimaryGeneratedColumn, Column, Index, CreateDateColumn } from 'typeorm';
import 'reflect-metadata';

@Entity()
export class Lookup {
  @Index({ unique: true })
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn()
  created_at!: Date;

  @Column({
    type: "enum",
    enum: ["country", "state"],
    nullable: true,
  })
  type!: string;

  @Column({
    nullable: true,
  })
  code!: string;

  @Column()
  name!: string;

  @Column({
    nullable: true,
  })
  flag!: string;

  @Column({
    nullable: true,
  })
  currency_symbol!: string;

  @Column({
    nullable: true,
  })
  order!: number;

  @Column({
    nullable: true,
  })
  active!: boolean;

  @Column("jsonb")
  content!: object;

  @Column({
    nullable: true,
  })
  version!: number;
}