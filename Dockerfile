# Dockerfile
FROM node:18

# Uygulama dizinini oluştur
WORKDIR /usr/src/app

# Paket ve bağımlılık dosyalarını kopyala
COPY package*.json ./

# Bağımlılıkları yükle
RUN npm install

# Uygulama kaynak kodunu kopyala
COPY . .

# TypeScript kodlarını derle
RUN npm run build
