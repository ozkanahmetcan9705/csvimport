//env parametrelerini json dosyalarına işler
import fs from 'fs';
import path from 'path';
import { loadEnvVariables } from './envLoader';

const env = loadEnvVariables();

const jsonConfigPath = path.resolve(__dirname, 'ormconfig.json');
const jsonConfigContent = fs.readFileSync(jsonConfigPath, 'utf8');

const updatedJsonConfigContent = jsonConfigContent.replace(/\${(\w+)}/g, (_, key) => env[key] || '');

const typeormConfig = JSON.parse(updatedJsonConfigContent);

export default typeormConfig;
