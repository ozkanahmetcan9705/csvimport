////env parametrelerini ts dosyalarına işler
import fs from 'fs';
import path from 'path';

export const loadEnvVariables = () => {
    const envFilePath = path.resolve(__dirname, '.env');
    const envFileContent = fs.readFileSync(envFilePath, 'utf8');
    const envVariables = envFileContent.split('\n');

    const env: any = {};
    envVariables.forEach(variable => {
        const [key, value] = variable.split('=').map(v => v.trim());
        if (key && value) {
            env[key] = value;
        }
    });
    return env;
};
