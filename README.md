# CSV Import

Bu proje, bir CSV dosyasından PostgreSQL veritabanına TypeORM kullanarak veri aktaran ve işlemleri RabbitMQ aracılığıyla kaydeden bir servistir. Uygulama, kolay dağıtım ve yönetim için Docker ile hazırlanmıştır.

## Kurulum

### Gereksinimler

- [Node.js](https://nodejs.org/) v18.17.1
- [Docker Desktop](https://www.docker.com/products/docker-desktop/)
- [PostgreSQL](https://www.postgresql.org/)
- [RabbitMQ](https://www.rabbitmq.com/)

### Adımlar

1. Depoyu klonlayın:
   ```sh
   git clone git@gitlab.com:ozkanahmetcan9705/csvimport.git
   cd csvimport

2. Bağımlılıkları yükleyin:
    ```sh
    npm install

## Yapılandırma

Uygulama içerisindeki ormconfig.json ve docker-compose.yml dosyaları dinamik bir şekilde .env dosyasına bağlıdır. Local portları ve docker portları .env dosyasından ayarlayabilirsiniz.

Ayarlanan çevre değişkenleri, database isimleri, portları, hostları aşağıda görüntülenen gibidir:

<img src="public/envpht.png" width="400">


Kendini kullanımınıza uygun olarak bu değişkenleri ayarlayabilirsiniz.

## Kullanım

Docker'ı kullanarak uygulamayı ayağa kaldırın:

    docker-compose up --build

http://localhost:8080/list adresinden tarayıcıda, docker postgresql veritabanına yazılmış verileri görüntüleyebilirsiniz:

<img src="public/list.png" width="400">


<img src="public/listtr.png" width="400">


Aşağıdaki komutu kullanarak uygulamayı, docker üzerinden down edebilirsiniz:

    docker-compose down

PostgreSQL çıktıları şu şekildedir:

<img src="public/postgresql.png" width="1000">

RabbitMQ logları şu şekildedir:

<img src="public/rabbitmq.png" width="1000">